library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity arithmetic_circuit_16bit_tb is
end arithmetic_circuit_16bit_tb;

architecture behavioural of arithmetic_circuit_16bit_tb is
  component arithmetic_circuit_16bit 
  port(a, b: in std_logic_vector(15 downto 0);
       s0, s1, Cin: in std_logic;
       g: out std_logic_vector(15 downto 0);
       Cout: out std_logic);
  end component;

  signal a, b, g: std_logic_vector(15 downto 0) := x"0000";
  signal s0, s1, Cin, Cout: std_logic := '0';

begin
  test_arithmetic_circuit_16bit: arithmetic_circuit_16bit
  port map(
          a => a,
          b => b,
          s0 => s0,
          s1 => s1,
          Cin => Cin,
          Cout => Cout,
          g => g
          );

  sim_proc: process
  begin
    Cin <= '0';
    a <= x"0080";
    b <= x"0080";
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    Cin <= '1';
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
