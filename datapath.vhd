library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity datapath is
  port( in_data, pc_in: in std_logic_vector(15 downto 0);
        Const: in std_logic_vector(2 downto 0);
        a_address, b_address, d_address: in std_logic_vector(3 downto 0);
        fs: in std_logic_vector(4 downto 0);
        clk, write, mb, md, mm: in std_logic;
        out_data, out_address: out std_logic_vector(15 downto 0);
        v, c, n, z: out std_logic);
end datapath;

architecture behavioural of datapath is
  component register_file
    port( clk, write: in std_logic;
          d_data: in std_logic_vector(15 downto 0);
          a_address, b_address, d_address: in std_logic_vector(3 downto 0);
          a_data, b_data: out std_logic_vector(15 downto 0));
  end component;

  component function_unit
    port( a, b: in std_logic_vector(15 downto 0);
          fs: in std_logic_vector(4 downto 0);
          f: out std_logic_vector(15 downto 0);
          v, c, n, z: out std_logic);
  end component;

  component mux2_16bit
    port( s: in std_logic;
    ln0, ln1: in std_logic_vector(15 downto 0);
    Z: out std_logic_vector(15 downto 0));
  end component;

  signal a_bus, b_bus, b_data, d_bus, f: std_logic_vector(15 downto 0) := x"0000";

begin
  rf: register_file
  port map(
    d_data => d_bus,
    a_data => a_bus,
    b_data => b_data,
    a_address => a_address,
    b_address => b_address,
    d_address => d_address,
    clk => clk,
    write => write
  );
  
  fu: function_unit
  port map(
    a => a_bus,
    b => b_bus,
    fs => fs,
    f => f,
    v => v,
    c => c,
    n => n,
    z => z
  );

  mux_b: mux2_16bit
  port map(
    s => mb,
    ln0 => b_data,
    ln1(2 downto 0) => Const,
    ln1(15 downto 3) => "0000000000000",
    Z => b_bus
  );

  mux_d: mux2_16bit
  port map(
    s => md,
    ln0 => f,
    ln1 => in_data,
    Z => d_bus
  );

  mux_m: mux2_16bit
  port map(
    s => mm,
    ln0 => a_bus,
    ln1 => pc_in,
    Z => out_address
  );

  out_data <= b_bus;
end behavioural;
