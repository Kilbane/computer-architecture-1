library ieee;
use ieee.std_logic_1164.all;

entity pc_tb is
end pc_tb;

architecture behavioural of pc_tb is
  component pc
    port( reset, load, increment, clk: in std_logic;
          offset: in std_logic_vector(15 downto 0);
          address: out std_logic_vector(15 downto 0));
  end component;

  signal reset, load, increment, clk: std_logic := '0';
  signal offset, address: std_logic_vector(15 downto 0) := x"0000";

begin
  test_pc: pc
  port map(
    reset => reset,
    load => load,
    increment => increment,
    clk => clk,
    offset => offset,
    address => address
  );

  clk_proc: process
  begin
    wait for 10 ns;
    clk <= not clk;
  end process;

  sim_proc: process
  begin
    reset <= '1';
    wait for 20 ns;
    reset <= '0';
    load <= '1';
    offset <= x"0003";
    wait for 20 ns;
    load <= '0';
    increment <= '1';
    wait for 20 ns;
    increment <= '0';
    wait for 20 ns;
    load <= '1';
    offset <= x"fffd";
    wait for 40 ns;
    load <= '0';
    increment <= '1';
    wait for 20 ns;
    reset <= '1';
    wait for 20 ns;
    reset <= '0';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
