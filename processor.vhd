library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity processor is
  port( clk, reset: std_logic);
end processor;

architecture behavioural of processor is
  component microprogrammed_control
    port( mw, mm, rw, md, mb, tb, ta, td: out std_logic;
          fs: out std_logic_vector(4 downto 0);
          dr, sa, sb: out std_logic_vector(2 downto 0);
          pc_out: out std_logic_vector(15 downto 0);
          v, c, n, z: in std_logic;
          ir_in: in std_logic_vector(15 downto 0);
          reset, clk: in std_logic);
  end component;

  component datapath
    port( in_data, pc_in: in std_logic_vector(15 downto 0);
          Const: in std_logic_vector(2 downto 0);
          a_address, b_address, d_address: in std_logic_vector(3 downto 0);
          fs: in std_logic_vector(4 downto 0);
          clk, write, mb, md, mm: in std_logic;
          out_data, out_address: out std_logic_vector(15 downto 0);
          v, c, n, z: out std_logic);
  end component;

  component memory
    port( address: in std_logic_vector(15 downto 0);
          data_in: in std_logic_vector(15 downto 0);
          mem_write: in std_logic;
          data_out: out std_logic_vector(15 downto 0));
  end component;
  
  signal mw, mm, rw, md, mb, tb, ta, td, v, c, n, z: std_logic;
  signal fs: std_logic_vector(4 downto 0);
  signal dr, sa, sb: std_logic_vector(2 downto 0);
  signal address, data_out, data_in, pc: std_logic_vector(15 downto 0);

begin
  micro: microprogrammed_control
  port map(
    mw => mw,
    mm => mm,
    rw => rw,
    md => md,
    fs => fs,
    mb => mb,
    tb => tb,
    ta => ta,
    td => td,
    dr => dr,
    sa => sa,
    sb => sb,
    pc_out => pc,
    v => v,
    c => c,
    n => n,
    z => z,
    ir_in => data_in,
    reset => reset,
    clk => clk
  );

  dp: datapath
  port map(
    in_data => data_in,
    pc_in => pc,
    Const => sb,
    a_address(2 downto 0) => sa,
    a_address(3) => ta,
    b_address(2 downto 0) => sb,
    b_address(3) => tb,
    d_address(2 downto 0) => dr,
    d_address(3) => td,
    fs => fs,
    clk => clk,
    write => rw,
    mb => mb,
    md => md,
    mm => mm,
    out_data => data_out,
    out_address => address,
    v => v,
    c => c,
    n => n,
    z => z
  );

  mem: memory
  port map(
    address => address,
    data_in => data_out,
    mem_write => mw,
    data_out => data_in
  );

end behavioural;
    
