library ieee;
use ieee.std_logic_1164.all;

entity shifter_16bit_tb is
end shifter_16bit_tb;

architecture behavioural of shifter_16bit_tb is
  component shifter_16bit is
    port(b: in std_logic_vector(15 downto 0);
         s: in std_logic_vector(1 downto 0);
         il, ir: in std_logic;
         h: out std_logic_vector(15 downto 0);
         outl, outr: out std_logic);
  end component;
  
  signal b, h: std_logic_vector(15 downto 0);
  signal s: std_logic_vector(1 downto 0);
  signal il, ir, outl, outr: std_logic;

begin
  test_shifter_16bit: shifter_16bit
  port map(
          b => b,
          s => s,
          il => il,
          ir => ir,
          h => h,
          outl => outl,
          outr => outr
          );

  sim_proc: process
  begin
    b <= x"AAAA";
    s <= "00";
    il <= '0';
    ir <= '0';
    wait for 20 ns;
    s <= "01";
    wait for 20 ns;
    s <= "10";
    wait for 20 ns;
    s <= "11";
    wait for 20 ns;
    wait;
  end process;
end behavioural;
