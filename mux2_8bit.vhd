library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity mux2_8bit is
        port( s: in std_logic;
        ln0, ln1: in std_logic_vector(7 downto 0);
        Z: out std_logic_vector(7 downto 0));
end mux2_8bit;

architecture behavioural of mux2_8bit is
begin
        Z <= ln0 after 1 ns when s = '0' else
             ln1 after 1 ns when s = '1' else
             x"00" after 1 ns;
end behavioural;
