library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity microprogrammed_control is
  port( mw, mm, rw, md, mb, tb, ta, td: out std_logic;
        fs: out std_logic_vector(4 downto 0);
        dr, sa, sb: out std_logic_vector(2 downto 0);
        pc_out: out std_logic_vector(15 downto 0);
        v, c, n, z: in std_logic;
        ir_in: in std_logic_vector(15 downto 0);
        reset, clk: in std_logic);
end microprogrammed_control;

architecture behavioural of microprogrammed_control is
  component pc
    port( reset, load, increment, clk: in std_logic;
          offset: in std_logic_vector(15 downto 0);
          address: out std_logic_vector(15 downto 0));
  end component;

  component instruction_reg
    port (load, clk: in std_logic;
      data: in std_logic_vector(15 downto 0);
      dr, sa, sb: out std_logic_vector(2 downto 0);
      opcode: out std_logic_vector(6 downto 0));
  end component;
                  
  component car
    port( reset, load, clk: in std_logic;
          data: in std_logic_vector(7 downto 0);
          address: out std_logic_vector(7 downto 0));
  end component;

  component mux8
    port(s: in std_logic_vector(2 downto 0);
         ln0, ln1, ln2, ln3, ln4, ln5, ln6, ln7: in std_logic;
         Z: out std_logic);
  end component;

  component mux2_8bit
          port( s: in std_logic;
          ln0, ln1: in std_logic_vector(7 downto 0);
          Z: out std_logic_vector(7 downto 0));
  end component;

  component control_memory
    port( mw: out std_logic;
          mm: out std_logic;
          rw: out std_logic;
          md: out std_logic;
          fs: out std_logic_vector(4 downto 0);
          mb: out std_logic;
          tb: out std_logic;
          ta: out std_logic;
          td: out std_logic;
          pl: out std_logic;
          pi: out std_logic;
          il: out std_logic;
          mc: out std_logic;
          ms: out std_logic_vector(2 downto 0);
          na: out std_logic_vector(7 downto 0);
          in_car: in std_logic_vector(7 downto 0));
  end component;

  signal pl, pi, il, mc: std_logic := '0';
  signal ms: std_logic_vector(2 downto 0) := "000";
  signal na: std_logic_vector(7 downto 0) := x"00";
  signal dr_signal, sb_signal: std_logic_vector(2 downto 0) := "000";
  signal opcode: std_logic_vector(6 downto 0) := "0000000";
  signal load_car: std_logic := '0';
  signal car_data, car_address: std_logic_vector(7 downto 0) := x"00";
  signal not_z, not_c: std_logic := '0';

begin
  program_counter: pc
  port map(
    clk => clk,
    reset => reset,
    load => pl,
    increment => pi,
    offset(2 downto 0) => sb_signal,
    offset(5 downto 3) => dr_signal,
    offset(6) => dr_signal(2),
    offset(7) => dr_signal(2),
    offset(8) => dr_signal(2),
    offset(9) => dr_signal(2),
    offset(10) => dr_signal(2),
    offset(11) => dr_signal(2),
    offset(12) => dr_signal(2),
    offset(13) => dr_signal(2),
    offset(14) => dr_signal(2),
    offset(15) => dr_signal(2),
    address => pc_out
  );

  ir: instruction_reg
  port map(
    clk => clk,
    load => il,
    data => ir_in,
    dr => dr_signal,
    sa => sa,
    sb => sb_signal,
    opcode => opcode
  );

  control_address_register: car
  port map(
    clk => clk,
    reset => reset,
    load => load_car,
    data => car_data,
    address => car_address
  );

  mux_s: mux8
  port map(
    s => ms,
    ln7 => not_z,
    ln6 => not_c,
    ln5 => n,
    ln4 => z,
    ln3 => v,
    ln2 => c,
    ln1 => '1',
    ln0 => '0',
    Z => load_car
  );

  mux_c: mux2_8bit
  port map(
    s => mc,
    ln0 => na,
    ln1(6 downto 0) => opcode,
    ln1(7) => '0',
    Z => car_data
  );

  control: control_memory
  port map(
    mw => mw,
    mm => mm,
    rw => rw,
    md => md,
    fs => fs,
    mb => mb,
    tb => tb,
    ta => ta,
    td => td,
    pl => pl,
    pi => pi,
    il => il,
    mc => mc,
    ms => ms,
    na => na,
    in_car => car_address
  );

  dr <= dr_signal;
  sb <= sb_signal;
  not_z <= not z;
  not_c <= not c;
end behavioural;
