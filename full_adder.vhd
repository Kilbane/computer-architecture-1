library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity full_adder is
        port(x, y, Cin: in std_logic;
                Cout, s: out std_logic);
end full_adder;

architecture behavioural of full_adder is
begin
        s <= x xor y xor Cin after 1 ns;
        Cout <= (x and y) or (x and Cin) or (y and Cin) after 1 ns;
end behavioural;
