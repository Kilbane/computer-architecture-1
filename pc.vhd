library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity pc is
  port( reset, load, increment, clk: in std_logic;
        offset: in std_logic_vector(15 downto 0);
        address: out std_logic_vector(15 downto 0));
end pc;

architecture behavioural of pc is
  signal current_address: std_logic_vector(15 downto 0) := x"0000";
begin process(clk, reset)
begin
  if reset = '1' then
    current_address <= x"0000";
  elsif(rising_edge(clk)) then
    if load = '1' then
      current_address <= current_address + offset;
    elsif increment = '1' then
      current_address <= current_address + '1';
    end if;
  end if;
end process;
  address <= current_address after 1 ns;
end behavioural; 
