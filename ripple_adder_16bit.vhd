library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ripple_adder_16bit is
  port(x, y: in std_logic_vector(15 downto 0);
       c0: in std_logic;
       s: out std_logic_vector(15 downto 0);
       c16: out std_logic);
end ripple_adder_16bit;

architecture behavioural of ripple_adder_16bit is
  component full_adder
    port(x, y, Cin: in std_logic;
          Cout, s: out std_logic);
  end component;

  signal c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15: std_logic;
begin
  fa00: full_adder
  port map(
          x => x(0),
          y => y(0),
          Cin => c0,
          Cout => c1,
          s => s(0)
          );

  fa01: full_adder
  port map(
          x => x(1),
          y => y(1),
          Cin => c1,
          Cout => c2,
          s => s(1)
          );

  fa02: full_adder
  port map(
          x => x(2),
          y => y(2),
          Cin => c2,
          Cout => c3,
          s => s(2)
          );

  fa03: full_adder
  port map(
          x => x(3),
          y => y(3),
          Cin => c3,
          Cout => c4,
          s => s(3)
          );

  fa04: full_adder
  port map(
          x => x(4),
          y => y(4),
          Cin => c4,
          Cout => c5,
          s => s(4)
          );

  fa05: full_adder
  port map(
          x => x(5),
          y => y(5),
          Cin => c5,
          Cout => c6,
          s => s(5)
          );

  fa06: full_adder
  port map(
          x => x(6),
          y => y(6),
          Cin => c6,
          Cout => c7,
          s => s(6)
          );

  fa07: full_adder
  port map(
          x => x(7),
          y => y(7),
          Cin => c7,
          Cout => c8,
          s => s(7)
          );

  fa08: full_adder
  port map(
          x => x(8),
          y => y(8),
          Cin => c8,
          Cout => c9,
          s => s(8)
          );

  fa09: full_adder
  port map(
          x => x(9),
          y => y(9),
          Cin => c9,
          Cout => c10,
          s => s(9)
          );

  fa10: full_adder
  port map(
          x => x(10),
          y => y(10),
          Cin => c10,
          Cout => c11,
          s => s(10)
          );

  fa11: full_adder
  port map(
          x => x(11),
          y => y(11),
          Cin => c11,
          Cout => c12,
          s => s(11)
          );

  fa12: full_adder
  port map(
          x => x(12),
          y => y(12),
          Cin => c12,
          Cout => c13,
          s => s(12)
          );

  fa13: full_adder
  port map(
          x => x(13),
          y => y(13),
          Cin => c13,
          Cout => c14,
          s => s(13)
          );

  fa14: full_adder
  port map(
          x => x(14),
          y => y(14),
          Cin => c14,
          Cout => c15,
          s => s(14)
          );

  fa15: full_adder
  port map(
          x => x(15),
          y => y(15),
          Cin => c15,
          Cout => c16,
          s => s(15)
          );
  
end behavioural;
