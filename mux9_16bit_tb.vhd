library ieee;
use ieee.std_logic_1164.all;

entity mux9_16bit_tb is
end mux9_16bit_tb;

architecture behavioural of mux9_16bit_tb is
  component mux9_16bit
    port(s: in std_logic_vector(3 downto 0);
      ln0, ln1, ln2, ln3, ln4, ln5, ln6, ln7, ln8: in std_logic_vector(15 downto 0);
      Z: out std_logic_vector(15 downto 0));
  end component;

  signal s: std_logic_vector(3 downto 0);
  signal Z: std_logic_vector(15 downto 0);
  signal ln0: std_logic_vector(15 downto 0) := x"0000";
  signal ln1: std_logic_vector(15 downto 0) := x"1111";
  signal ln2: std_logic_vector(15 downto 0) := x"2222";
  signal ln3: std_logic_vector(15 downto 0) := x"3333";
  signal ln4: std_logic_vector(15 downto 0) := x"4444";
  signal ln5: std_logic_vector(15 downto 0) := x"5555";
  signal ln6: std_logic_vector(15 downto 0) := x"6666";
  signal ln7: std_logic_vector(15 downto 0) := x"7777";
  signal ln8: std_logic_vector(15 downto 0) := x"8888";

begin
  test_mux9_16bit: mux9_16bit
  port map(
    s => s,
    ln0 => ln0,
    ln1 => ln1,
    ln2 => ln2,
    ln3 => ln3,
    ln4 => ln4,
    ln5 => ln5,
    ln6 => ln6,
    ln7 => ln7,
    ln8 => ln8,
    Z => Z
  );

  sim_proc: process
  begin
    s <= "0000";
    wait for 20 ns;
    s <= "0001";
    wait for 20 ns;
    s <= "0010";
    wait for 20 ns;
    s <= "0011";
    wait for 20 ns;
    s <= "0100";
    wait for 20 ns;
    s <= "0101";
    wait for 20 ns;
    s <= "0110";
    wait for 20 ns;
    s <= "0111";
    wait for 20 ns;
    s <= "1000";
    wait for 20 ns;
    s <= "1001";
    wait for 20 ns;
    s <= "1010";
    wait for 20 ns;
    s <= "1011";
    wait for 20 ns;
    s <= "1100";
    wait for 20 ns;
    s <= "1101";
    wait for 20 ns;
    s <= "1110";
    wait for 20 ns;
    s <= "1111";
    wait for 20 ns;
    wait;
  end process;
end behavioural;
