library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity reg16 is
  port (load, clk: in std_logic;
    D: in std_logic_vector(15 downto 0);
    Q: out std_logic_vector(15 downto 0));
end reg16;
                
architecture behavioural of reg16 is
begin process(clk)
begin
  if(rising_edge(clk)) then
    if load='1' then
      Q <= D after 1 ns;
    end if;
  end if;
end process;
end behavioural;
