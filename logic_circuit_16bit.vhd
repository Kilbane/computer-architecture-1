library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity logic_circuit_16bit is
  port(a, b: in std_logic_vector(15 downto 0);
       s0, s1: in std_logic;
       g: out std_logic_vector(15 downto 0));
end logic_circuit_16bit;

architecture behavioural of logic_circuit_16bit is
  component logic_circuit_slice
    port(a, b, s0, s1: in std_logic;
         g: out std_logic);
  end component;

begin
  lc00: logic_circuit_slice
  port map(
          a => a(0),
          b => b(0),
          s0 => s0,
          s1 => s1,
          g => g(0)
          );
    
  lc01: logic_circuit_slice
  port map(
          a => a(1),
          b => b(1),
          s0 => s0,
          s1 => s1,
          g => g(1)
          );
    
  lc02: logic_circuit_slice
  port map(
          a => a(2),
          b => b(2),
          s0 => s0,
          s1 => s1,
          g => g(2)
          );
    
  lc03: logic_circuit_slice
  port map(
          a => a(3),
          b => b(3),
          s0 => s0,
          s1 => s1,
          g => g(3)
          );
    
  lc04: logic_circuit_slice
  port map(
          a => a(4),
          b => b(4),
          s0 => s0,
          s1 => s1,
          g => g(4)
          );
    
  lc05: logic_circuit_slice
  port map(
          a => a(5),
          b => b(5),
          s0 => s0,
          s1 => s1,
          g => g(5)
          );
    
  lc06: logic_circuit_slice
  port map(
          a => a(6),
          b => b(6),
          s0 => s0,
          s1 => s1,
          g => g(6)
          );
    
  lc07: logic_circuit_slice
  port map(
          a => a(7),
          b => b(7),
          s0 => s0,
          s1 => s1,
          g => g(7)
          );
    
  lc08: logic_circuit_slice
  port map(
          a => a(8),
          b => b(8),
          s0 => s0,
          s1 => s1,
          g => g(8)
          );
    
  lc09: logic_circuit_slice
  port map(
          a => a(9),
          b => b(9),
          s0 => s0,
          s1 => s1,
          g => g(9)
          );
    
  lc10: logic_circuit_slice
  port map(
          a => a(10),
          b => b(10),
          s0 => s0,
          s1 => s1,
          g => g(10)
          );
    
  lc11: logic_circuit_slice
  port map(
          a => a(11),
          b => b(11),
          s0 => s0,
          s1 => s1,
          g => g(11)
          );
    
  lc12: logic_circuit_slice
  port map(
          a => a(12),
          b => b(12),
          s0 => s0,
          s1 => s1,
          g => g(12)
          );
    
  lc13: logic_circuit_slice
  port map(
          a => a(13),
          b => b(13),
          s0 => s0,
          s1 => s1,
          g => g(13)
          );
    
  lc14: logic_circuit_slice
  port map(
          a => a(14),
          b => b(14),
          s0 => s0,
          s1 => s1,
          g => g(14)
          );
    
  lc15: logic_circuit_slice
  port map(
          a => a(15),
          b => b(15),
          s0 => s0,
          s1 => s1,
          g => g(15)
          );
    
end behavioural;
