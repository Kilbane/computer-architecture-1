library ieee;
use ieee.std_logic_1164.all;

entity register_file_tb is
end register_file_tb;

architecture behavioural of register_file_tb is
  component register_file
    port( clk, write: in std_logic;
          d_data: in std_logic_vector(15 downto 0);
          a_address, b_address, d_address: in std_logic_vector(3 downto 0);
          a_data, b_data: out std_logic_vector(15 downto 0));
  end component; 

  signal clk, write: std_logic :='0';
  signal a_address, b_address, d_address: std_logic_vector(3 downto 0) :="0000";
  signal a_data, b_data, d_data: std_logic_vector(15 downto 0) :=x"0000";

begin
  test_register_file: register_file
  port map(
    clk => clk,
    write => write,
    d_data => d_data,
    a_data => a_data,
    b_data => b_data,
    d_address => d_address,
    a_address => a_address,
    b_address => b_address
  );

    clk_proc: process
    begin
      wait for 10 ns;
      clk <= not clk;
    end process;

    sim_proc: process
    begin 
      d_address <= "0011";
      d_data <= x"ABCD";
      a_address <= "0011";
      wait for 20 ns;
      write <= '1';
      wait for 20 ns;
      write <= '0';
      d_address <= "0111";
      d_data <= x"1234";
      b_address <= "0111";
      wait for 20 ns;
      write <= '1';
      wait for 20 ns;
      a_address <= "0111";
      b_address <= "0011";
      wait for 20 ns;
      d_data <= x"DEAD";
      d_address <= "1000";
      a_address <= "1001";
      b_address <= "1010";
      wait for 20 ns;
      wait;
    end process;
end behavioural;
