library ieee;
use ieee.std_logic_1164.all;

entity full_adder_tb is
end full_adder_tb;

architecture behavioural of full_adder_tb is
        component full_adder
                port(x, y, Cin: in std_logic;
                        Cout, s: out std_logic);
        end component;

        signal x, y, Cin, Cout, s: std_logic;

begin
        test_full_adder: full_adder
                port map(
                        x => x,
                        y => y,
                        Cin => Cin,
                        Cout => Cout,
                        s => s
                        );

        sim_proc: process
        begin
                x <= '0';
                y <= '0';
                Cin <= '0';
                wait for 20 ns;

                Cin <= '1';
                wait for 20 ns;

                y <= '1';
                Cin <= '0';
                wait for 20 ns;

                Cin <= '1';
                wait for 20 ns;

                x <= '1';
                y <= '0';
                Cin <= '0';
                wait for 20 ns;

                Cin <= '1';
                wait for 20 ns;

                y <= '1';
                Cin <= '0';
                wait for 20 ns;

                Cin <= '1';
                wait for 20 ns;
                wait;

        end process;
end behavioural;
