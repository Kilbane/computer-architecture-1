library ieee;
use ieee.std_logic_1164.all;

entity logic_circuit_16bit_tb is
end logic_circuit_16bit_tb;

architecture behavioural of logic_circuit_16bit_tb is
  component logic_circuit_16bit
    port(a, b: in std_logic_vector(15 downto 0);
         s0, s1: in std_logic;
         g: out std_logic_vector(15 downto 0));
  end component;
  
  signal a, b, g: std_logic_vector(15 downto 0);
  signal s0, s1: std_logic;

begin
  test_logic_circuit_16bit: logic_circuit_16bit
  port map(
          a => a,
          b => b,
          s0 => s0,
          s1 => s1,
          g => g
          );

  sim_proc: process
  begin
    a <= x"0000";
    b <= x"0000";
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    a <= x"0000";
    b <= x"FFFF";
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    a <= x"FFFF";
    b <= x"0000";
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    a <= x"FFFF";
    b <= x"FFFF";
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    s0 <= '1';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
