library ieee;
use ieee.std_logic_1164.all;

entity control_memory_tb is
end control_memory_tb;

architecture behavioural of control_memory_tb is
  component control_memory
    port( mw: out std_logic;
          mm: out std_logic;
          rw: out std_logic;
          md: out std_logic;
          fs: out std_logic_vector(4 downto 0);
          mb: out std_logic;
          tb: out std_logic;
          ta: out std_logic;
          td: out std_logic;
          pl: out std_logic;
          pi: out std_logic;
          il: out std_logic;
          mc: out std_logic;
          ms: out std_logic_vector(2 downto 0);
          na: out std_logic_vector(7 downto 0);
          in_car: in std_logic_vector(7 downto 0));
  end component;
  
  signal mw, mm, rw, md, mb, tb, ta, td, pl, pi, il, mc: std_logic;
  signal fs: std_logic_vector(4 downto 0);
  signal ms: std_logic_vector(2 downto 0);
  signal na, in_car: std_logic_vector(7 downto 0);

begin
  test_control_memory: control_memory
  port map(
    mw => mw,
    mm => mm,
    rw => rw,
    md => md,
    fs => fs,
    mb => mb,
    tb => tb,
    ta => ta,
    td => td,
    pl => pl,
    pi => pi,
    il => il,
    mc => mc,
    ms => ms,
    na => na,
    in_car => in_car
  );

  sim_proc: process
  begin
    in_car <= x"00";
    wait for 20 ns;
    in_car <= x"01";
    wait for 20 ns;
    in_car <= x"02";
    wait for 20 ns;
    in_car <= x"03";
    wait for 20 ns;
    in_car <= x"04";
    wait for 20 ns;
    in_car <= x"05";
    wait for 20 ns;
    in_car <= x"06";
    wait for 20 ns;
    in_car <= x"07";
    wait for 20 ns;
    in_car <= x"08";
    wait for 20 ns;
    in_car <= x"09";
    wait for 20 ns;
    in_car <= x"0a";
    wait for 20 ns;
    in_car <= x"0b";
    wait for 20 ns;
    in_car <= x"0c";
    wait for 20 ns;
    in_car <= x"0d";
    wait for 20 ns;
    in_car <= x"0e";
    wait for 20 ns;
    in_car <= x"0f";
    wait for 20 ns;
    wait;
  end process;
end behavioural;
