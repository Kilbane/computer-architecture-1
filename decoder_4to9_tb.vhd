library ieee;
use ieee.std_logic_1164.all;

entity decoder_4to9_tb is
end decoder_4to9_tb;

architecture behavioural of decoder_4to9_tb is
  component decoder_4to9
    port( A: in std_logic_vector(3 downto 0);
          Q: out std_logic_vector(8 downto 0));
  end component;

  signal A: std_logic_vector(3 downto 0);
  signal Q: std_logic_vector(8 downto 0);
begin
  test_decoder_4to9: decoder_4to9
  port map(
    A => A,
    Q => Q
  );

  sim_proc: process
  begin
    A <= "0000";
    wait for 10 ns;
    A <= "0001";
    wait for 10 ns;
    A <= "0010";
    wait for 10 ns;
    A <= "0011";
    wait for 10 ns;
    A <= "0100";
    wait for 10 ns;
    A <= "0101";
    wait for 10 ns;
    A <= "0110";
    wait for 10 ns;
    A <= "0111";
    wait for 10 ns;
    A <= "1000";
    wait for 10 ns;
    A <= "1001";
    wait for 10 ns;
    A <= "1010";
    wait for 10 ns;
    A <= "1011";
    wait for 10 ns;
    A <= "1100";
    wait for 10 ns;
    A <= "1101";
    wait for 10 ns;
    A <= "1110";
    wait for 10 ns;
    A <= "1111";
    wait for 10 ns;
    wait;
  end process;

end behavioural;
