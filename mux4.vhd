library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity mux4 is
  port(s0, s1, ln0, ln1, ln2, ln3: in std_logic;
       Z: out std_logic);
end mux4;

architecture behavioural of mux4 is
begin
  Z <= ln0 after 1 ns when s0='0' and s1='0' else
       ln1 after 1 ns when s0='1' and s1='0' else
       ln2 after 1 ns when s0='0' and s1='1' else
       ln3 after 1 ns when s0='1' and s1='1' else
       '0' after 1 ns;
end behavioural;
