library ieee;
use ieee.std_logic_1164.all;

entity reg16_tb is
end reg16_tb;

architecture behavioural of reg16_tb is
  component reg16
    port (load, clk: in std_logic;
      D: in std_logic_vector(15 downto 0);
      Q: out std_logic_vector(15 downto 0));
  end component;

  signal load, clk: std_logic := '0';
  signal D, Q: std_logic_vector(15 downto 0);

begin
  test_reg16: reg16
  port map(
    load => load,
    clk => clk,
    D => D,
    Q => Q
  );
  
  clk_proc: process
  begin
    wait for 10 ns;
    clk <= not clk;
  end process;

  sim_proc: process
  begin
    D <= x"bead";
    wait for 20 ns;
    load <= '1';
    wait for 20 ns;
    D <= x"face";
    wait for 20 ns;
    load <= '0';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
