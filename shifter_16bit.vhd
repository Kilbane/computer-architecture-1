library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity shifter_16bit is
  port(b: in std_logic_vector(15 downto 0);
       s: in std_logic_vector(1 downto 0);
       il, ir: in std_logic;
       h: out std_logic_vector(15 downto 0);
       outl, outr: out std_logic);
end shifter_16bit;

architecture behavioural of shifter_16bit is
  component mux4
    port(s0, s1, ln0, ln1, ln2, ln3: in std_logic;
         Z: out std_logic);
  end component;

  signal ln3: std_logic := '0';

begin
  mux00: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(0),
          ln1 => b(1),
          ln2 => il,
          ln3 => ln3,
          Z => h(0)
          );

  mux01: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(1),
          ln1 => b(2),
          ln2 => b(0),
          ln3 => ln3,
          Z => h(1)
          );
  
  mux02: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(2),
          ln1 => b(3),
          ln2 => b(1),
          ln3 => ln3,
          Z => h(2)
          );
  
  mux03: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(3),
          ln1 => b(4),
          ln2 => b(2),
          ln3 => ln3,
          Z => h(3)
          );
  
  mux04: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(4),
          ln1 => b(5),
          ln2 => b(3),
          ln3 => ln3,
          Z => h(4)
          );
  
  mux05: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(5),
          ln1 => b(6),
          ln2 => b(4),
          ln3 => ln3,
          Z => h(5)
          );
  
  mux06: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(6),
          ln1 => b(7),
          ln2 => b(5),
          ln3 => ln3,
          Z => h(6)
          );
  
  mux07: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(7),
          ln1 => b(8),
          ln2 => b(6),
          ln3 => ln3,
          Z => h(7)
          );
  
  mux08: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(8),
          ln1 => b(9),
          ln2 => b(7),
          ln3 => ln3,
          Z => h(8)
          );
  
  mux09: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(9),
          ln1 => b(10),
          ln2 => b(8),
          ln3 => ln3,
          Z => h(9)
          );
  
  mux10: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(10),
          ln1 => b(11),
          ln2 => b(9),
          ln3 => ln3,
          Z => h(10)
          );
  
  mux11: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(11),
          ln1 => b(12),
          ln2 => b(10),
          ln3 => ln3,
          Z => h(11)
          );
  
  mux12: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(12),
          ln1 => b(13),
          ln2 => b(11),
          ln3 => ln3,
          Z => h(12)
          );
  
  mux13: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(13),
          ln1 => b(14),
          ln2 => b(12),
          ln3 => ln3,
          Z => h(13)
          );
  
  mux14: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(14),
          ln1 => b(15),
          ln2 => b(13),
          ln3 => ln3,
          Z => h(14)
          );

  mux15: mux4
  port map(
          s0 => s(0),
          s1 => s(1),
          ln0 => b(15),
          ln1 => ir,
          ln2 => b(14),
          ln3 => ln3,
          Z => h(15)
          );

  outr <= b(0) after 1 ns;
  outl <= b(15) after 1 ns;

end behavioural;

