library ieee;
use ieee.std_logic_1164.all;

entity instruction_reg_tb is
end instruction_reg_tb;

architecture behavioural of instruction_reg_tb is
  component instruction_reg
    port (load, clk: in std_logic;
      data: in std_logic_vector(15 downto 0);
      dr, sa, sb: out std_logic_vector(2 downto 0);
      opcode: out std_logic_vector(6 downto 0));
  end component;

  signal load, clk: std_logic := '0';
  signal data: std_logic_vector(15 downto 0);
  signal opcode: std_logic_vector(6 downto 0);
  signal dr, sa, sb: std_logic_vector(2 downto 0);

begin
  test_instruction_reg: instruction_reg
  port map(
    load => load,
    clk => clk,
    data => data,
    dr => dr,
    sa => sa,
    sb => sb,
    opcode => opcode
  );
  
  clk_proc: process
  begin
    wait for 10 ns;
    clk <= not clk;
  end process;

  sim_proc: process
  begin
    data <= x"bead";
    wait for 20 ns;
    load <= '1';
    wait for 20 ns;
    data <= x"face";
    wait for 20 ns;
    load <= '0';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
