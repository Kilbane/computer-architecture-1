library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity function_unit is
  port( a, b: in std_logic_vector(15 downto 0);
        fs: in std_logic_vector(4 downto 0);
        f: out std_logic_vector(15 downto 0);
        v, c, n, z: out std_logic);
end function_unit;

architecture behavioural of function_unit is
  component alu_16bit
    port(a, b: in std_logic_vector(15 downto 0);
         Cin, s0, s1, s2: in std_logic;
         g: out std_logic_vector(15 downto 0);
         Cout: out std_logic);
  end component;

  component shifter_16bit
    port(b: in std_logic_vector(15 downto 0);
         s: in std_logic_vector(1 downto 0);
         il, ir: in std_logic;
         h: out std_logic_vector(15 downto 0);
         outl, outr: out std_logic);
  end component;

  component mux2_16bit is
          port( s: in std_logic;
          ln0, ln1: in std_logic_vector(15 downto 0);
          Z: out std_logic_vector(15 downto 0));
  end component; 

  signal g, h: std_logic_vector(15 downto 0) :=x"0000";
  signal il, ir, Cout: std_logic :='0';

begin
  alu: alu_16bit
  port map(
          a => a,
          b => b,
          Cin => fs(0),
          s0 => fs(1),
          s1 => fs(2),
          s2 => fs(3),
          g => g,
          Cout => Cout
          );

  shifter: shifter_16bit
  port map(
          b => b,
          s => fs(3 downto 2),
          il => il,
          ir => ir,
          h => h
          );
  
  muxf: mux2_16bit
  port map(
          s => fs(4),
          ln0 => g,
          ln1 => h,
          Z => f
          );

  v <= '1' after 1 ns when ((Cout xor (a(14) and b(14))) and (not fs(4)))='1' else
       '0' after 1 ns;
  c <= '1' after 1 ns when (Cout and (not fs(4)))='1' else
       '0' after 1 ns;
  n <= '1' after 1 ns when g(15)='1' and  fs(4)='0' else
       '0' after 1 ns;
  z <= '1' after 1 ns when g="0000000000000000" and fs(4)='0' else
       '0' after 1 ns;
  
end behavioural;
