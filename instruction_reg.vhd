library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity instruction_reg is
  port (load, clk: in std_logic;
    data: in std_logic_vector(15 downto 0);
    dr, sa, sb: out std_logic_vector(2 downto 0);
    opcode: out std_logic_vector(6 downto 0));
end instruction_reg;
                
architecture behavioural of instruction_reg is
  signal sb_signal, sa_signal, dr_signal: std_logic_vector(2 downto 0) := "000";
  signal opcode_signal:  std_logic_vector(6 downto 0):= "0000000";
begin process(clk)
begin
  if(rising_edge(clk)) then
    if load = '1' then
      sb_signal <= data(2 downto 0);
      sa_signal <= data(5 downto 3);
      dr_signal <= data(8 downto 6);
      opcode_signal <= data(15 downto 9);
    end if;
  end if;
end process;
sb <= sb_signal after 1 ns;
sa <= sa_signal after 1 ns;
dr <= dr_signal after 1 ns;
opcode <= opcode_signal after 1 ns;
end behavioural;
