library ieee;
use ieee.std_logic_1164.all;

entity car_tb is
end car_tb;

architecture behavioural of car_tb is
  component car
    port( reset, load, clk: in std_logic;
          data: in std_logic_vector(7 downto 0);
          address: out std_logic_vector(7 downto 0));
  end component;

  signal reset, load, clk: std_logic := '0';
  signal data, address: std_logic_vector(7 downto 0) := x"00";
  
begin
  test_car: car
  port map(
    reset => reset,
    load => load,
    clk => clk,
    data => data,
    address => address
  );

  clk_proc: process
  begin
    wait for 10 ns;
    clk <= not clk;
  end process;
  
  sim_proc: process
  begin
    data <= x"07";
    wait for 20 ns;
    load <= '1';
    wait for 20 ns;
    data <= x"45";
    wait for 20 ns;
    reset <= '1';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
