.PHONY: default all clean

MODULES = alu_16bit arithmetic_circuit_16bit car control_memory datapath decoder_4to9 full_adder function_unit input_logic_16bit \
	  input_logic_slice instruction_reg logic_circuit_16bit logic_circuit_slice memory microprogrammed_control mux2_16bit \
	  mux2_8bit mux4 mux8 mux8_16bit mux9_16bit pc processor reg16 register_file ripple_adder_16bit shifter_16bit

BENCHES = alu_16bit_tb arithmetic_circuit_16bit_tb car_tb control_memory_tb datapath_tb decoder_4to9_tb full_adder_tb \
	  function_unit_tb input_logic_16bit_tb input_logic_slice_tb instruction_reg_tb logic_circuit_16bit_tb \
	  logic_circuit_slice_tb memory_tb mux2_16bit_tb microprogrammed_control_tb mux2_8bit_tb mux4_tb mux8_tb mux8_16bit_tb \
	  mux9_16bit_tb pc_tb processor_tb reg16_tb register_file_tb ripple_adder_16bit_tb shifter_16bit_tb

default: all
all: $(BENCHES)

$(MODULES:=.o): %.o: %.vhd
	ghdl -a -fexplicit -o $@ -fsynopsys $< 

$(BENCHES:=.o): %.o: %.vhd
	ghdl -a -fexplicit -o $@ -fsynopsys $< 

$(BENCHES): %: %.o | $(MODULES:=.o)
	ghdl -e -fexplicit -o $@ -fsynopsys $(<:.o=)
	
run_%: %
	ghdl -r $< --vcd-4states --vcd=$<.vcd

clean:
	-rm -f *.o
	-rm -f $(BENCHES)
	-rm -f *.vcd
	-rm -f work-*.cf
