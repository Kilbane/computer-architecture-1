library ieee;
use ieee.std_logic_1164.all;

entity microprogrammed_control_tb is
end microprogrammed_control_tb;


architecture behavioural of microprogrammed_control_tb is
  component microprogrammed_control
    port( mw, mm, rw, md, mb, tb, ta, td: out std_logic;
          fs: out std_logic_vector(4 downto 0);
          dr, sa, sb: out std_logic_vector(2 downto 0);
          pc_out: out std_logic_vector(15 downto 0);
          v, c, n, z: in std_logic;
          ir_in: in std_logic_vector(15 downto 0);
          reset, clk: in std_logic);
  end component;

  signal reset, clk, mw, mm, rw, md, mb, tb, ta, td, v, c, n, z: std_logic := '0';
  signal fs: std_logic_vector(4 downto 0) := "00000";
  signal dr, sa, sb: std_logic_vector(2 downto 0) := "000";
  signal pc_out, ir_in: std_logic_vector(15 downto 0) := x"0000";

begin
  test_microprogrammed_control: microprogrammed_control
  port map( clk => clk,
            reset => reset,
            mw => mw,
            mm => mm,
            rw => rw,
            md => md,
            mb => mb,
            tb => tb,
            ta => ta,
            td => td,
            v => v,
            c => c,
            n => n,
            z => z,
            fs => fs,
            dr => dr,
            sa => sa,
            sb => sb,
            pc_out => pc_out,
            ir_in => ir_in
  );

  clk_proc: process
  begin
    wait for 10 ns;
    clk <= not clk;
  end process;

  sim_proc: process
  begin
    reset <= '1';
    wait for 20 ns;
    reset <= '0';
    v <= '0';
    c <= '0';
    n <= '0';
    z <= '0';
    ir_in <= x"0000";
    wait for 20 ns;
    wait;
  end process;
end behavioural;
