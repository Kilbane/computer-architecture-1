library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity register_file is
  port( clk, write: in std_logic;
        d_data: in std_logic_vector(15 downto 0);
        a_address, b_address, d_address: in std_logic_vector(3 downto 0);
        a_data, b_data: out std_logic_vector(15 downto 0));
end register_file;

architecture behavioural of register_file is
  component reg16
    port (load, clk: in std_logic;
      D: in std_logic_vector(15 downto 0);
      Q: out std_logic_vector(15 downto 0));
  end component;

  component decoder_4to9
    port( A: in std_logic_vector(3 downto 0);
          Q: out std_logic_vector(8 downto 0));
    end component;

  component mux9_16bit
    port(s: in std_logic_vector(3 downto 0);
         ln0, ln1, ln2, ln3, ln4, ln5, ln6, ln7, ln8: in std_logic_vector(15 downto 0);
         Z: out std_logic_vector(15 downto 0));
  end component;

  signal load_reg0, load_reg1, load_reg2, load_reg3, load_reg4, load_reg5, load_reg6, load_reg7, load_reg8,
         select_reg0, select_reg1, select_reg2, select_reg3, select_reg4, select_reg5, select_reg6, select_reg7, select_reg8: std_logic :='0';
  signal reg0_q, reg1_q, reg2_q, reg3_q, reg4_q, reg5_q, reg6_q, reg7_q, reg8_q: std_logic_vector(15 downto 0) :=x"0000";
  
begin
  reg00: reg16
  port map(
    D => d_data,
    load => load_reg0,
    clk => clk,
    Q => reg0_q 
  );

  reg01: reg16
  port map(
    D => d_data,
    load => load_reg1,
    clk => clk,
    Q => reg1_q 
  );

  reg02: reg16
  port map(
    D => d_data,
    load => load_reg2,
    clk => clk,
    Q => reg2_q 
  );

  reg03: reg16
  port map(
    D => d_data,
    load => load_reg3,
    clk => clk,
    Q => reg3_q 
  );

  reg04: reg16
  port map(
    D => d_data,
    load => load_reg4,
    clk => clk,
    Q => reg4_q 
  );

  reg05: reg16
  port map(
    D => d_data,
    load => load_reg5,
    clk => clk,
    Q => reg5_q 
  );

  reg06: reg16
  port map(
    D => d_data,
    load => load_reg6,
    clk => clk,
    Q => reg6_q 
  );

  reg07: reg16
  port map(
    D => d_data,
    load => load_reg7,
    clk => clk,
    Q => reg7_q 
  );

  reg08: reg16
  port map(
    D => d_data,
    load => load_reg8,
    clk => clk,
    Q => reg8_q 
  );

  decoder: decoder_4to9
  port map(
    A => d_address,
    Q(0) => select_reg0,
    Q(1) => select_reg1,
    Q(2) => select_reg2,
    Q(3) => select_reg3,
    Q(4) => select_reg4,
    Q(5) => select_reg5,
    Q(6) => select_reg6,
    Q(7) => select_reg7,
    Q(8) => select_reg8
  );

  a_mux: mux9_16bit
  port map(
    ln0 => reg0_q,
    ln1 => reg1_q,
    ln2 => reg2_q,
    ln3 => reg3_q,
    ln4 => reg4_q,
    ln5 => reg5_q,
    ln6 => reg6_q,
    ln7 => reg7_q,
    ln8 => reg8_q,
    s => a_address,
    Z => a_data
  );

  b_mux: mux9_16bit
  port map(
    ln0 => reg0_q,
    ln1 => reg1_q,
    ln2 => reg2_q,
    ln3 => reg3_q,
    ln4 => reg4_q,
    ln5 => reg5_q,
    ln6 => reg6_q,
    ln7 => reg7_q,
    ln8 => reg8_q,
    s => b_address,
    Z => b_data
  );
  
  load_reg0 <= write and select_reg0;
  load_reg1 <= write and select_reg1;
  load_reg2 <= write and select_reg2;
  load_reg3 <= write and select_reg3;
  load_reg4 <= write and select_reg4;
  load_reg5 <= write and select_reg5;
  load_reg6 <= write and select_reg6;
  load_reg7 <= write and select_reg7;
  load_reg8 <= write and select_reg8;
  
end behavioural;
