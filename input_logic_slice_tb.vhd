library ieee;
use ieee.std_logic_1164.all;

entity input_logic_slice_tb is
end input_logic_slice_tb;

architecture behavioural of input_logic_slice_tb is
  component input_logic_slice
    port(b, s0, s1: in std_logic;
         y: out std_logic);
  end component;

  signal b, s0, s1, y: std_logic;
  
begin
  test_input_logic_slice: input_logic_slice
    port map(
            b => b,
            s0 => s0,
            s1 => s1,
            y => y
            );

  sim_proc: process
  begin
    -- b = '0' s0 = '0' s1 = '0'
    b <= '0';
    s0 <= '0';
    s1 <= '0';
    wait for 10 ns;
    -- b = '1' s0 = '0' s1 = '0'
    b <= '1';
    s0 <= '0';
    s1 <= '0';
    wait for 10 ns;
    -- b = '0' s0 = '1' s1 = '0'
    b <= '0';
    s0 <= '1';
    s1 <= '0';
    wait for 10 ns;
    -- b = '1' s0 = '1' s1 = '0'
    b <= '1';
    s0 <= '1';
    s1 <= '0';
    wait for 10 ns;
    -- b = '0' s0 = '0' s1 = '1'
    b <= '0';
    s0 <= '0';
    s1 <= '1';
    wait for 10 ns;
    -- b = '1' s0 = '0' s1 = '1'
    b <= '1';
    s0 <= '0';
    s1 <= '1';
    wait for 10 ns;
    -- b = '0' s0 = '1' s1 = '1'
    b <= '0';
    s0 <= '1';
    s1 <= '1';
    wait for 10 ns;
    -- b = '1' s0 = '1' s1 = '1'
    b <= '1';
    s0 <= '1';
    s1 <= '1';
    wait for 10 ns;
    wait;
  end process;
end behavioural;
        
