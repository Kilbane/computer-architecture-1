library ieee;
use ieee.std_logic_1164.all;

entity logic_circuit_slice_tb is
end logic_circuit_slice_tb;

architecture behavioural of logic_circuit_slice_tb is
  component logic_circuit_slice
    port(a, b, s0, s1: in std_logic;
         g: out std_logic);
  end component;

  signal a, b, s0, s1, g: std_logic := '0';

begin
  test_logic_circuit_slice: logic_circuit_slice
  port map(
          a => a,
          b => b,
          s0 => s0,
          s1 => s1,
          g => g
          );

  sim_proc: process
  begin
    a <= '0';
    b <= '0';
    s0 <= '0';
    s1 <= '0';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    a <= '0';
    b <= '1';
    s0 <= '0';
    s1 <= '0';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    a <= '1';
    b <= '0';
    s0 <= '0';
    s1 <= '0';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    a <= '1';
    b <= '1';
    s0 <= '0';
    s1 <= '0';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    s0 <= '0';
    s1 <= '1';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    wait;
  end process;
end behavioural;
