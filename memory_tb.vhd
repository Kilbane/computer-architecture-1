library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity memory_tb is
end memory_tb;

architecture behavioural of memory_tb is
  component memory
    port( address: in std_logic_vector(15 downto 0);
          data_in: in std_logic_vector(15 downto 0);
          mem_write: in std_logic;
          data_out: out std_logic_vector(15 downto 0));
  end component;

  signal address: std_logic_vector(15 downto 0);
  signal data_in, data_out: std_logic_vector(15 downto 0);
  signal mem_write: std_logic;
  
begin
  test_memory: memory
  port map(
    address => address,
    data_in => data_in,
    data_out => data_out,
    mem_write => mem_write
  );

  sim_proc: process
  begin
    mem_write <= '1';
    address <= x"0000";
    data_in <= x"0001";
    wait for 20 ns;
    address <= x"0001";
    data_in <= x"0002";
    wait for 20 ns;
    address <= x"0002";
    data_in <= x"0003";
    wait for 20 ns;
    address <= x"0003";
    data_in <= x"0004";
    wait for 20 ns;
    mem_write <= '0';
    address <= x"0000";
    wait for 20 ns;
    address <= x"0001";
    wait for 20 ns;
    address <= x"0002";
    wait for 20 ns;
    address <= x"0003";
    wait for 20 ns;
    wait;
  end process;
end behavioural;
