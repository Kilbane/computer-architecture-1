library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity input_logic_slice is
  port(b, s0, s1: in std_logic;
       y: out std_logic);
end input_logic_slice;

architecture behavioural of input_logic_slice is
begin
  y <= (s0 and b) or (s1 and (not b)) after 1 ns;
end behavioural;
