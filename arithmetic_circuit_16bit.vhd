library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity arithmetic_circuit_16bit is
  port(a, b: in std_logic_vector(15 downto 0);
       s0, s1, Cin: in std_logic;
       g: out std_logic_vector(15 downto 0);
       Cout: out std_logic);
end arithmetic_circuit_16bit;

architecture behavioural of arithmetic_circuit_16bit is
  component ripple_adder_16bit
    port(x, y: in std_logic_vector(15 downto 0);
         c0: in std_logic;
         s: out std_logic_vector(15 downto 0);
         c16: out std_logic);
  end component;

  component input_logic_16bit
    port(b: in std_logic_vector(15 downto 0);
         s0, s1: in std_logic;
         y: out std_logic_vector(15 downto 0));
  end component;

  signal y: std_logic_vector(15 downto 0); 

begin
  ripple_adder: ripple_adder_16bit
  port map(
          x => a,
          y => y,
          s => g,
          c0 => Cin,
          c16 => Cout
          );

  input_logic: input_logic_16bit
  port map(
          b => b,
          y => y,
          s0 => s0,
          s1 => s1
          );
end behavioural;
          
