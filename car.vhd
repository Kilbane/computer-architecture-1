library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity car is
  port( reset, load, clk: in std_logic;
        data: in std_logic_vector(7 downto 0);
        address: out std_logic_vector(7 downto 0));
end car;

architecture behavioural of car is
  signal current_address: std_logic_vector(7 downto 0) := x"00";
begin process(clk, reset)
begin
  if reset = '1' then
    current_address <= x"00";
  elsif(rising_edge(clk)) then
    if load = '1' then
      current_address <= data;
    else
      current_address <= current_address + '1';
    end if;
  end if;
end process;
  address <= current_address after 1 ns;
end behavioural;
        
