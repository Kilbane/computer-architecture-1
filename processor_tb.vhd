library ieee;
use ieee.std_logic_1164.all;

entity processor_tb is
end processor_tb;

architecture behavioural of processor_tb is
  component processor
    port( clk, reset: std_logic);
  end component;

  signal clk, reset: std_logic := '0';

begin
  test_processor: processor
  port map(
    clk => clk,
    reset => reset
  );

  clk_proc: process
  begin
    wait for 10 ns;
    clk <= not clk;
  end process;

  sim_proc: process
  begin
    reset <= '1';
    wait for 20 ns;
    reset <= '0';
    wait for 200 ns;
    wait;
  end process;
end behavioural;
