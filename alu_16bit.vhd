library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity alu_16bit is
  port(a, b: in std_logic_vector(15 downto 0);
       Cin, s0, s1, s2: in std_logic;
       g: out std_logic_vector(15 downto 0);
       Cout: out std_logic);
end alu_16bit;

architecture behavioural of alu_16bit is
  component arithmetic_circuit_16bit
    port(a, b: in std_logic_vector(15 downto 0);
         s0, s1, Cin: in std_logic;
         g: out std_logic_vector(15 downto 0);
         Cout: out std_logic);
  end component;

  component logic_circuit_16bit is
    port(a, b: in std_logic_vector(15 downto 0);
         s0, s1: in std_logic;
         g: out std_logic_vector(15 downto 0));
  end component;
  
  component mux2_16bit is
        port( s: in std_logic;
        ln0, ln1: in std_logic_vector(15 downto 0);
        Z: out std_logic_vector(15 downto 0));
  end component;

  signal arith, logic: std_logic_vector(15 downto 0) := x"0000";

begin
  ac: arithmetic_circuit_16bit
  port map(
          a => a,
          b => b,
          s0 => s0,
          s1 => s1,
          Cin => Cin,
          Cout => Cout,
          g => arith
          );

  lc: logic_circuit_16bit
  port map(
          a => a,
          b => b,
          s0 => s0,
          s1 => s1,
          g => logic
          );

  mux: mux2_16bit
  port map(
          s => s2,
          ln0 => arith,
          ln1 => logic,
          Z => g
          );

end behavioural;

