library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity decoder_4to9 is
  port( A: in std_logic_vector(3 downto 0);
        Q: out std_logic_vector(8 downto 0));
end decoder_4to9;

architecture behavioural of decoder_4to9 is
begin
  Q <= "000000001" after 1 ns when A = "0000" else
       "000000010" after 1 ns when A = "0001" else
       "000000100" after 1 ns when A = "0010" else
       "000001000" after 1 ns when A = "0011" else
       "000010000" after 1 ns when A = "0100" else
       "000100000" after 1 ns when A = "0101" else
       "001000000" after 1 ns when A = "0110" else
       "010000000" after 1 ns when A = "0111" else
       "100000000" after 1 ns when A(3) = '1' else
       "000000000" after 1 ns;
end behavioural;
