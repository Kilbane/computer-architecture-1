library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity logic_circuit_slice is
  port(a, b, s0, s1: in std_logic;
       g: out std_logic);
end logic_circuit_slice;

architecture behavioural of logic_circuit_slice is
begin
  g <= a and b after 1 ns when s0='0' and s1='0' else
       a or b after 1 ns when s0='1' and s1='0' else
       a xor b after 1 ns when s0='0' and s1='1' else
       not a after 1 ns when s0='1' and s1='1' else
       '0' after 1 ns;
end behavioural;
