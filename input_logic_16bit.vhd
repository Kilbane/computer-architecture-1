library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity input_logic_16bit is
  port(b: in std_logic_vector(15 downto 0);
       s0, s1: in std_logic;
       y: out std_logic_vector(15 downto 0));
end input_logic_16bit;

architecture behavioural of input_logic_16bit is
  component input_logic_slice
    port(b, s0, s1: in std_logic;
         y: out std_logic);
  end component;

begin
  slice00: input_logic_slice
  port map(
          b => b(0),
          y => y(0),
          s0 => s0,
          s1 => s1
          );

  slice01: input_logic_slice
  port map(
          b => b(1),
          y => y(1),
          s0 => s0,
          s1 => s1
          );

  slice02: input_logic_slice
  port map(
          b => b(2),
          y => y(2),
          s0 => s0,
          s1 => s1
          );

  slice03: input_logic_slice
  port map(
          b => b(3),
          y => y(3),
          s0 => s0,
          s1 => s1
          );

  slice04: input_logic_slice
  port map(
          b => b(4),
          y => y(4),
          s0 => s0,
          s1 => s1
          );

  slice05: input_logic_slice
  port map(
          b => b(5),
          y => y(5),
          s0 => s0,
          s1 => s1
          );

  slice06: input_logic_slice
  port map(
          b => b(6),
          y => y(6),
          s0 => s0,
          s1 => s1
          );

  slice07: input_logic_slice
  port map(
          b => b(7),
          y => y(7),
          s0 => s0,
          s1 => s1
          );

  slice08: input_logic_slice
  port map(
          b => b(8),
          y => y(8),
          s0 => s0,
          s1 => s1
          );

  slice09: input_logic_slice
  port map(
          b => b(9),
          y => y(9),
          s0 => s0,
          s1 => s1
          );

  slice10: input_logic_slice
  port map(
          b => b(10),
          y => y(10),
          s0 => s0,
          s1 => s1
          );

  slice11: input_logic_slice
  port map(
          b => b(11),
          y => y(11),
          s0 => s0,
          s1 => s1
          );

  slice12: input_logic_slice
  port map(
          b => b(12),
          y => y(12),
          s0 => s0,
          s1 => s1
          );

  slice13: input_logic_slice
  port map(
          b => b(13),
          y => y(13),
          s0 => s0,
          s1 => s1
          );

  slice14: input_logic_slice
  port map(
          b => b(14),
          y => y(14),
          s0 => s0,
          s1 => s1
          );

  slice15: input_logic_slice
  port map(
          b => b(15),
          y => y(15),
          s0 => s0,
          s1 => s1
          );

end behavioural;
