library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity mux8 is
  port(s: in std_logic_vector(2 downto 0);
       ln0, ln1, ln2, ln3, ln4, ln5, ln6, ln7: in std_logic;
       Z: out std_logic);
end mux8;

architecture behavioural of mux8 is
begin
  Z <= ln0 after 1 ns when s="000" else
       ln1 after 1 ns when s="001" else
       ln2 after 1 ns when s="010" else
       ln3 after 1 ns when s="011" else
       ln4 after 1 ns when s="100" else
       ln5 after 1 ns when s="101" else
       ln6 after 1 ns when s="110" else
       ln7 after 1 ns when s="111" else
       '0' after 1 ns;
end behavioural;
