library ieee;
use ieee.std_logic_1164.all;

entity input_logic_16bit_tb is
end input_logic_16bit_tb;

architecture behavioural of input_logic_16bit_tb is
  component input_logic_16bit is
    port(b: in std_logic_vector(15 downto 0);
         s0, s1: in std_logic;
         y: out std_logic_vector(15 downto 0));
  end component;

  signal b, y: std_logic_vector(15 downto 0) := x"0000";
  signal s0, s1: std_logic :='0';

begin
  test_input_logic_16bit: input_logic_16bit
    port map(
            b => b,
            y => y,
            s0 => s0,
            s1 => s1
            );
    
  sim_proc: process
  begin
    -- b = 0x0000 s0 = '0' s1 = '0'
    b <= x"0000";
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    -- b = 0x0000 s0 = '1' s1 = '0'
    s0 <= '1';
    wait for 20 ns;
    -- b = 0x0000 s0 = '0' s1 = '1'
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    -- b = 0x0000 s0 = '1' s1 = '1'
    s0 <= '1';
    wait for 20 ns;
    -- b = 0xFFFF s0 = '0' s1 = '0'
    b <= x"FFFF";
    s0 <= '0';
    s1 <= '0';
    wait for 20 ns;
    -- b = 0xFFFF s0 = '1' s1 = '0'
    s0 <= '1';
    wait for 20 ns;
    -- b = 0xFFFF s0 = '0' s1 = '1'
    s0 <= '0';
    s1 <= '1';
    wait for 20 ns;
    -- b = 0xFFFF s0 = '1' s1 = '1'
    s0 <= '1';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
