library ieee;
use ieee.std_logic_1164.all;

entity mux8_tb is
end mux8_tb;

architecture behavioural of mux8_tb is
  component mux8
    port(s: in std_logic_vector(2 downto 0);
      ln0, ln1, ln2, ln3, ln4, ln5, ln6, ln7: in std_logic;
      Z: out std_logic);
  end component;

  signal s: std_logic_vector(2 downto 0);
  signal Z: std_logic;
  signal ln0: std_logic := '0';
  signal ln1: std_logic := '1';
  signal ln2: std_logic := '0';
  signal ln3: std_logic := '1';
  signal ln4: std_logic := '0';
  signal ln5: std_logic := '1';
  signal ln6: std_logic := '1';
  signal ln7: std_logic := '0';

begin
        test_mux8: mux8
        port map(
          s => s,
          ln0 => ln0,
          ln1 => ln1,
          ln2 => ln2,
          ln3 => ln3,
          ln4 => ln4,
          ln5 => ln5,
          ln6 => ln6,
          ln7 => ln7,
          Z => Z
        );

        sim_proc: process
        begin
          s <= "000";
          wait for 20 ns;
          s <= "001";
          wait for 20 ns;
          s <= "010";
          wait for 20 ns;
          s <= "011";
          wait for 20 ns;
          s <= "100";
          wait for 20 ns;
          s <= "101";
          wait for 20 ns;
          s <= "110";
          wait for 20 ns;
          s <= "111";
          wait for 20 ns;
          wait;
        end process;
end behavioural;
