library ieee;
use ieee.std_logic_1164.all;

entity mux4_tb is
end mux4_tb;

architecture behavioural of mux4_tb is
  component mux4 
    port(s0, s1, ln0, ln1, ln2, ln3: in std_logic;
         Z: out std_logic);
  end component;
  
  signal s0, s1, ln0, ln1, ln2, ln3, Z: std_logic;

begin
  test_mux4: mux4
  port map(
          s0 => s0,
          s1 => s1,
          ln0 => ln0,
          ln1 => ln1,
          ln2 => ln2,
          ln3 => ln3,
          Z => Z
          );

  sim_proc: process
  begin
    s0 <= '0';
    s1 <= '0';
    ln0 <= '0';
    ln1 <= '1';
    ln2 <= '0';
    ln3 <= '1';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    s0 <= '0';
    s1 <= '1';
    ln0 <= '0';
    ln1 <= '1';
    ln2 <= '0';
    ln3 <= '1';
    wait for 10 ns;
    s0 <= '1';
    wait for 10 ns;
    wait;
  end process;
end behavioural;

