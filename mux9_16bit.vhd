library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity mux9_16bit is
  port(s: in std_logic_vector(3 downto 0);
       ln0, ln1, ln2, ln3, ln4, ln5, ln6, ln7, ln8: in std_logic_vector(15 downto 0);
       Z: out std_logic_vector(15 downto 0));
end mux9_16bit;

architecture behavioural of mux9_16bit is
begin
  Z <= ln0 after 1 ns when s = "0000" else
       ln1 after 1 ns when s = "0001" else
       ln2 after 1 ns when s = "0010" else
       ln3 after 1 ns when s = "0011" else
       ln4 after 1 ns when s = "0100" else
       ln5 after 1 ns when s = "0101" else
       ln6 after 1 ns when s = "0110" else
       ln7 after 1 ns when s = "0111" else
       ln8 after 1 ns when s(3) = '1' else
       x"0000" after 1 ns;
end behavioural;
