library ieee;
use ieee.std_logic_1164.all;

entity function_unit_tb is
end function_unit_tb;

architecture behavioural of function_unit_tb is
  component function_unit
    port( a, b: in std_logic_vector(15 downto 0);
          fs: in std_logic_vector(4 downto 0);
          f: out std_logic_vector(15 downto 0);
          v, c, n, z: out std_logic);
  end component;
  
  signal a, b, f: std_logic_vector(15 downto 0) :=x"0000";
  signal fs: std_logic_vector(4 downto 0) :="00000";
  signal v, c, n, z: std_logic :='0';

begin
  fu: function_unit
  port map(
          a => a,
          b => b,
          f => f,
          fs => fs,
          v => v,
          c => c,
          n => n,
          z => z
          );

  sim_proc: process
  begin
    a <= x"F0F0";
    b <= x"0F0F";
    fs <= "00000";
    wait for 20 ns;
    fs <= "00001";
    wait for 20 ns;
    fs <= "00010";
    wait for 20 ns;
    fs <= "00011";
    wait for 20 ns;
    fs <= "00100";
    wait for 20 ns;
    fs <= "00101";
    wait for 20 ns;
    fs <= "00110";
    wait for 20 ns;
    fs <= "00111";
    wait for 20 ns;
    fs <= "01000";
    wait for 20 ns;
    fs <= "01010";
    wait for 20 ns;
    fs <= "01100";
    wait for 20 ns;
    fs <= "01110";
    wait for 20 ns;
    fs <= "10000";
    wait for 20 ns;
    fs <= "10100";
    wait for 20 ns;
    fs <= "11000";
    wait for 20 ns;
    wait;
  end process;
end behavioural;
