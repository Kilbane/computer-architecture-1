library ieee;
use ieee.std_logic_1164.all;

entity mux2_8bit_tb is
end mux2_8bit_tb;

architecture behavioural of mux2_8bit_tb is
  component mux2_8bit is
    port( s: in std_logic;
    ln0, ln1: in std_logic_vector(7 downto 0);
    Z: out std_logic_vector(7 downto 0));
  end component;

  signal s: std_logic;
  signal ln0: std_logic_vector(7 downto 0) := x"ca";
  signal ln1: std_logic_vector(7 downto 0) := x"fe";
  signal Z: std_logic_vector(7 downto 0);
begin
  test_mux2_8bit: mux2_8bit
  port map(
    s => s,
    ln0 => ln0,
    ln1 => ln1,
    Z => Z
  );

  sim_proc: process
  begin
    s <= '0';
    wait for 20 ns;

    s <= '1';
    wait for 20 ns;
    wait;
  end process;
end behavioural;
