library ieee;
use ieee.std_logic_1164.all;

entity ripple_adder_16bit_tb is
end ripple_adder_16bit_tb;

architecture behavioural of ripple_adder_16bit_tb is
  component ripple_adder_16bit
    port(x, y: in std_logic_vector(15 downto 0);
         c0: in std_logic;
         s: out std_logic_vector(15 downto 0);
         c16: out std_logic);
  end component;

  signal c0, c16: std_logic :='0';
  signal x, y, s: std_logic_vector(15 downto 0) := x"0000";
  
begin
  test_ripple_adder_16bit: ripple_adder_16bit
    port map(
            x => x,
            y => y,
            s => s,
            c0 => c0,
            c16 => c16
            );

    sim_proc: process
    begin
      -- x = 0xFFFF y = 0x0001 c0 = '0'
      x <= x"FFFF";
      y <= x"0001";
      wait for 20 ns;
      wait;

    end process;

end behavioural;
