library ieee;
use ieee.std_logic_1164.all;

entity datapath_tb is
end datapath_tb;

architecture behavioural of datapath_tb is
  component datapath
    port( in_data, pc_in: in std_logic_vector(15 downto 0);
          Const: in std_logic_vector(2 downto 0);
          a_address, b_address, d_address: in std_logic_vector(3 downto 0);
          fs: in std_logic_vector(4 downto 0);
          clk, write, mb, md, mm: in std_logic;
          out_data, out_address: out std_logic_vector(15 downto 0);
          v, c, n, z: out std_logic);
  end component;

  signal in_data, pc_in, out_data, out_address: std_logic_vector(15 downto 0) := x"0000";
  signal Const: std_logic_vector(2 downto 0) := "000";
  signal fs: std_logic_vector(4 downto 0) := "00000";
  signal a_address, b_address, d_address: std_logic_vector(3 downto 0) :="0000";
  signal clk, write, mb, md, mm, v, c, n, z: std_logic :='0';

begin
  test_datapath: datapath
  port map(
    in_data => in_data,
    pc_in => pc_in,
    Const => Const,
    out_data => out_data,
    out_address => out_address,
    fs => fs,
    a_address => a_address,
    b_address => b_address,
    d_address => d_address,
    clk => clk,
    write => write,
    mb => mb,
    md => md,
    mm => mm,
    v => v,
    c => c,
    n => n,
    z => z
  );

  clk_proc: process
  begin
    wait for 10 ns;
    clk <= not clk;
  end process;

  sim_proc: process
  begin
    -- R0 <= x"1111"
    d_address <= "0000";
    in_data <= x"1111";
    md <= '1';
    write <= '1';
    a_address <= "0000";
    b_address <= "0000";
    fs <= "00000";
    wait for 20 ns;
    -- R1 <= R0 + 1
    d_address <= "0001";
    b_address <= "0001";
    md <= '0';
    fs <= "00001";
    wait for 20 ns;
    -- R2 <= R0 + R1
    d_address <= "0010";
    fs <= "00010";
    wait for 20 ns;
    -- R3 <= R2 + !R1
    d_address <= "0011";
    a_address <= "0010";
    fs <= "00010";
    wait for 20 ns;
    pc_in <= x"0099";
    mm <= '1';
    wait for 20 ns;
    Const <= "101";
    mb <= '1';
    wait for 20 ns;
    wait;
  end process;
end behavioural;

